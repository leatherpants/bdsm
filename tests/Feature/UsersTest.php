<?php

namespace Tests\Feature;

use Tests\TestCase;

class UsersTest extends TestCase
{
    /** @test */
    public function test_shows_profile()
    {
        $response = $this->actingAs($this->user)->get(route('user', $this->user->username));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_profile_fails()
    {
        $response = $this->actingAs($this->user)->get(route('user', 'johndoe'));
        $response->assertNotFound();
    }
}
