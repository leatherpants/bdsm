<?php

namespace Tests\Feature;

use Tests\TestCase;

class HomeTest extends TestCase
{
    /** @test */
    public function test_shows_the_landing_page()
    {
        $response = $this->get(route('index'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_user_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('home'));
        $response->assertSuccessful();
    }
}
