<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\BDSM\Users\Repository\UsersRepository;

class UsersController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * UsersController constructor.
     * @param UsersRepository $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    /**
     * Shows the user profile.
     * @param string $username
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile(string $username)
    {
        $user = $this->usersRepository->getProfileByUsername($username);

        if (!$user) {
            abort(404);
        }

        return view('users.profile')
            ->with('user', $user);
    }
}
