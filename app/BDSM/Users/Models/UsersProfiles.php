<?php

namespace App\BDSM\Users\Models;

use Illuminate\Database\Eloquent\Model;

class UsersProfiles extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'bio',
    ];

    /**
     * Users relationship.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(Users::class);
    }
}
