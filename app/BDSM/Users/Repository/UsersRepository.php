<?php

namespace App\BDSM\Users\Repository;

use App\BDSM\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Get data for the user profile.
     * @param string $username
     * @return mixed
     */
    public function getProfileByUsername(string $username)
    {
        return $this->users
            ->where('username', $username)
            ->first();
    }
}
