<?php

namespace App\BDSM\Users\Repository;

use App\BDSM\Users\Models\UsersProfiles;

class UsersProfilesRepository
{
    /**
     * @var UsersProfiles
     */
    private $usersProfiles;

    /**
     * UsersProfilesRepository constructor.
     * @param UsersProfiles $usersProfiles
     */
    public function __construct(UsersProfiles $usersProfiles)
    {
        $this->usersProfiles = $usersProfiles;
    }
}
