<?php

use Faker\Generator as Faker;

$factory->define(\App\BDSM\Users\Models\Users::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'username' => $faker->unique()->userName,
        'sex' => $faker->randomElements(['male', 'female'])[0],
        'remember_token' => str_random(10),
    ];
});
