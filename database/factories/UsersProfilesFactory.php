<?php

use Faker\Generator as Faker;

$factory->define(\App\BDSM\Users\Models\UsersProfiles::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(\App\BDSM\Users\Models\Users::class)->create()->id;
        },
        'bio' => $faker->text,
    ];
});
