<?php

return [
    'login' => 'Log in',
    'login_msg' => 'Log in to BDSM',
    'logout' => 'Log out',
    'password' => 'Password',
    'username' => 'Username',
];
