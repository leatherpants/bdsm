@extends('layouts.blank')
@section('title', 'BDSM')
@section('content')
    <div class="container mx-auto p-8">
        <div class="mx-auto max-w-sm">
            <div class="bg-white rounded shadow">
                <div class="border-b py-8 font-bold text-black text-center text-xl tracking-widest uppercase">{{ trans('common.login_msg') }}</div>
                <form action="{{ route('login') }}" method="post" class="bg-grey-lightest px-10 py-10">
                    @csrf

                    <div class="mb-3">
                        <input type="text" name="username" id="username" class="border w-full p-3" placeholder="{{ trans('common.username') }}">
                    </div>
                    <div class="mb-6">
                        <input type="password" name="password" id="password" class="border w-full p-3" placeholder="{{ trans('common.password') }}">
                    </div>
                    <div class="flex">
                        <button class="bg-teal hover:bg-primary-dark w-full p-4 text-sm text-white uppercase font-bold tracking-wider">{{ trans('common.login') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection