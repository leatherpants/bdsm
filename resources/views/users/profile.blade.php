@extends('layouts.frontend')
@section('title', 'BDSM - '.$user->username)
@section('content')
    <div class="container mx-auto">
        <div class="flex flex-wrap">
            <div class="w-full md:w-1/4">
                <img src="https://cdn3.cdnme.se/cdn/8-2/147625/images/2009/untitled-1_55453679.jpg" class="w-full" alt="{{ $user->username }}">
            </div>
            <div class="w-full md:w-3/4 my-3 px-3">
                <div class="flex-col flex">
                    <h1 class="text-white">{{ $user->username }}</h1>
                    <div class="text-sm text-grey-dark mb-4">{{ $user->sex }}</div>
                </div>
                <div class="flex-col flex">
                    @if ($user->profile)
                    {!! $user->profile->bio !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection