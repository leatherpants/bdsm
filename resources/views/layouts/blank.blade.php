<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{ asset('assets/main.css') }}" rel="stylesheet">
</head>
<body class="bg-grey-darkest font-sans text-white">
@yield('content')
</body>
</html>