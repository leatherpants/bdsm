<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'index']);

    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', ['uses' => 'LoginController@showLoginForm', 'as' => 'login']);
        Route::post('login', ['uses' => 'LoginController@login']);
        Route::post('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });

    Route::group(['middleware' => ['web']], function () {
        Route::get('home', ['uses' => 'HomeController@home', 'as' => 'home']);

        Route::group(['prefix' => 'users'], function () {
            Route::get('{username}', ['uses' => 'UsersController@profile', 'as' => 'user']);
        });
    });
});
